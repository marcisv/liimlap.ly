module.exports = (app) ->

  app.get '/', require('./app/actions/show_home')
  app.get '/desktops/:desktop_id', require('./app/actions/show_home')

  app.post   '/desktops', require('./app/actions/create_desktop')
  app.delete '/desktops/:id', require('./app/actions/destroy_desktop')

  app.post   '/desktops/:desktop_id/notes', require('./app/actions/create_note')
  app.put    '/desktops/:desktop_id/notes/:id', require('./app/actions/update_note')
  app.delete '/desktops/:desktop_id/notes/:id', require('./app/actions/destroy_note')
