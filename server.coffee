express = require('express')

assets = require('connect-assets')
hamlc = require('haml-coffee')

mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/liimlap')

app = express()

partials = require('express-partials')
partials.register('.hamlc', hamlc.render)

app.engine 'hamlc', hamlc.__express
app.configure ->
  app.set 'view engine', 'hamlc'
  app.set('views', __dirname + '/app/assets/js/templates')
  app.use partials()

  app.use assets
    src: 'app/assets'
    buildDir: 'public'
    jsCompilers:
      hamlc:
        match: /\.hamlc$/
        compileSync: (sourcePath, source) ->
          templateName = sourcePath.split('assets/js/templates/')[1].split('.hamlc')[0]
          hamlc.template source, templateName, 'window.JST'

  app.use express.static(__dirname + '/public')
  app.use express.bodyParser()
  app.use app.router

require('./routes') app

port = process.env.PORT || 3000
app.listen port
console.log 'Listening on http://0.0.0.0:' + port
