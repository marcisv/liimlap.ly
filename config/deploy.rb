set :application, 'liimlap'
set :repo_url, 'git@bitbucket.org:marcisv/liimlap.ly.git'

set :branch, 'master'

set :deploy_to, -> { "/home/#{fetch(:user)}/production" }

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app) do
      execute :mkdir, '-p', release_path.join('tmp')
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, 'npm:update'
  after :publishing, 'deploy:restart'
  after :finishing,  'deploy:cleanup'

end
