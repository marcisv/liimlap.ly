Desktop = require('../models/desktop')

module.exports = (req, res) ->
  Desktop.findById req.params.desktop_id, (err, desktop) ->
    if err
      res.send(400)
    else
      note = desktop.notes.id(req.params.id)
      return res.send 400 unless note
      for attribute in ['kind', 'text', 'imageUrl', 'xPosition', 'yPosition']
        if req.body[attribute] != undefined
          note[attribute] = req.body[attribute]
      desktop.save (err) ->
        res.send if err then 400 else note.publicAttributes()
