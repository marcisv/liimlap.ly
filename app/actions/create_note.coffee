Desktop = require('../models/desktop')

module.exports = (req, res) ->
  Desktop.findById req.params.desktop_id, (err, desktop) ->
    if err
      res.send(400)
    else
      noteCount = desktop.notes.push
        kind:      req.body.kind
        text:      req.body.text
        imageUrl:  req.body.imageUrl
        xPosition: req.body.xPosition
        yPosition: req.body.yPosition
      desktop.save (err) ->
        res.send if err then 400 else desktop.notes[noteCount-1].publicAttributes()
