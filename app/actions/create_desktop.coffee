Desktop = require('../models/desktop')

module.exports = (req, res) ->
  Desktop.create title: req.body.title, (err, desktop) ->
    if err
      res.send(400)
    else
      res.send desktop.publicAttributes()
