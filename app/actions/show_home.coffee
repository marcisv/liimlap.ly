Desktop = require('../models/desktop')

module.exports = (req, res) ->
  currentDesktopId = req.params.desktop_id
  Desktop.find().sort(created_at: 1).exec (err, desktops) ->
    desktopAttributes = desktops.map (desktop) ->
      attributes = desktop.publicAttributes()
      if attributes.id.toString() == currentDesktopId
        attributes.current = true
      attributes
    res.render 'layout',
      desktops:       desktopAttributes
      currentDesktop: (desktopAttributes.filter (d) -> d.current)[0] || desktopAttributes[0]
