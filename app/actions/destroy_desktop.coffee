Desktop = require('../models/desktop')

module.exports = (req, res) ->
  Desktop.findByIdAndRemove req.params.id, (err, desktop) ->
    if err
      res.send 400
    else
      res.send {}
