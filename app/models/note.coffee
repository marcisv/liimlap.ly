mongoose = require('mongoose')

Schema = mongoose.Schema

Note = new Schema
  text: String
  imageUrl: String
  kind: {type: String, enum: ['image', 'text'], required: true}
  xPosition: {type: Number, required: true}
  yPosition: {type: Number, required: true}

Note.methods.publicAttributes = ->
  {
    id: @_id
    text: @text
    imageUrl: @imageUrl
    kind: @kind
    xPosition: @xPosition
    yPosition: @yPosition
    desktopId: @parent()._id
  }

module.exports = mongoose.model('Note', Note);
