mongoose = require('mongoose')
Note = require('./note')

Schema = mongoose.Schema

Desktop = new Schema
  title:      {type: String, required: true}
  notes:      [Note.schema]
  created_at: {type: Date, default: Date.now}

Desktop.methods.publicAttributes = ->
  {
    id: @_id
    title: @title
    notes: @notes.map (note) -> note.publicAttributes()
  }

module.exports = mongoose.model('Desktop', Desktop);
