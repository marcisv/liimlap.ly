StickyNotes.Desktop = Backbone.Model.extend

  urlRoot: '/desktops'

  noteList: null

  initialize: ->
    @noteList = new StickyNotes.NoteList(@get('notes'))

  notes: ->
    @noteList.models
