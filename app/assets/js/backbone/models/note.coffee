StickyNotes.Note = Backbone.Model.extend

  urlRoot: -> '/desktops/' + @get('desktopId') + '/notes/'

  updatePosition: (offset, containerOffset, containerWidth, containerHeight) ->
    topOffset = offset.top - containerOffset.top
    leftOffset = offset.left - containerOffset.left
    @set 'xPosition', leftOffset / (containerWidth / 100)
    @set 'yPosition', topOffset / (containerHeight / 100)
    @save()
