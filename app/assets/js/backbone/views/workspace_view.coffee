StickyNotes.WorkspaceView = Backbone.View.extend

  el: 'body'

  render: ->
    @$el.html JST['workspaces/show']()
    new StickyNotes.DesktopListView(collection: @collection)
