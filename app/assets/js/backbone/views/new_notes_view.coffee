StickyNotes.NewNotesView = Backbone.View.extend

  id: 'new-notes'

  initialize: (options) ->
    @desktop = options.desktop
    @render()

  render: ->
    @$el.html JST['notes/new']()
    @bindDraggableNotes()
    @

  $desktop: -> $('#current-desktop-wrapper')

  bindDraggableNotes: ->
    @$('.note-container').draggable
      helper: 'clone'
      scroll: false
      stop: (event, ui) =>
        droppedOffset = ui.helper.offset()
        if @isNotePositionValid $(event.target), droppedOffset
          @note = new StickyNotes.Note desktopId: @desktop.id, kind: $(event.target).data('kind')
          @note.updatePosition droppedOffset, @$desktop().offset(), @$desktop().width(), @$desktop().height()
          @desktop.noteList.add @note

  isNotePositionValid: ($noteContainer, offset) ->
    minLeft = @$desktop().offset().left
    maxLeft = minLeft + @$desktop().width() - $noteContainer.width()
    minTop = @$desktop().offset().top
    maxTop = minTop + @$desktop().height() - $noteContainer.width()
    minLeft < offset.left < maxLeft && minTop < offset.top < maxTop
