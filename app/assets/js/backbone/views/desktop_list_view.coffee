StickyNotes.DesktopListView = Backbone.View.extend

  currentDesktop: null

  el: 'ul#desktops'

  events:
    'click .add-new-desktop'   : 'handleNewDesktopClick'

  initialize: ->
    _.bindAll @, 'addToList', 'renderIfFirstAdded', 'resetIfCurrentDestroyed'
    @collection.on 'add', @addToList
    @collection.on 'add', @renderIfFirstAdded
    @collection.on 'destroy', @resetIfCurrentDestroyed
    @render()

  render: ->
    @$el.empty()
    @$el.append JST['desktops/add_button']()
    _.each @collection.models, (desktop) =>
      @addToList(desktop)
      @renderDesktop(desktop) if desktop.get('current')
    @resetCurrentDesktop() unless @currentDesktop

  $currentDesktopWrapper: -> $('#current-desktop-wrapper')

  $newNotesWrapper: -> $('#new-notes-wrapper')

  handleNewDesktopClick: (e) ->
    e.preventDefault()
    $(e.currentTarget).closest('li').before new StickyNotes.NewDesktopView(collection: @collection).render().el

  addToList: (desktop) ->
    listItemView = new StickyNotes.DesktopListItemView(model: desktop, listView: @)
    @$('.add-new-desktop').closest('li').before listItemView.render().el

  resetCurrentDesktop: ->
    if @collection.models.length
      @renderDesktop @collection.models[0]
    else
      @removeDesktopViews()

  renderIfFirstAdded: (desktop) ->
    if @collection.length == 1
      @renderDesktop(desktop)

  resetIfCurrentDestroyed: (desktop) ->
    if desktop == @currentDesktop
      @resetCurrentDesktop()

  renderDesktop: (desktop) ->
    @currentDesktop = desktop
    @removeDesktopViews()

    @currentDesktopView = new StickyNotes.DesktopView model: @currentDesktop
    @$currentDesktopWrapper().html @currentDesktopView.el

    @newNotesView = new StickyNotes.NewNotesView collection: @collection, desktop: @currentDesktop
    @$newNotesWrapper().html @newNotesView.el

    desktop.trigger 'set-as-current'

  removeDesktopViews: ->
    @newNotesView && @newNotesView.remove()
    @currentDesktopView && @currentDesktopView.remove()
