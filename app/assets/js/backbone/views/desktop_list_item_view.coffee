StickyNotes.DesktopListItemView = Backbone.View.extend

  tagName: 'li'

  events:
    'click' : 'handleElClick'
    'click .destroy-desktop' : 'handleDestroyClick'

  listView: null

  initialize: (options) ->
    @listView = options.listView
    _.bindAll @, 'highLightEl'
    @model.on 'set-as-current', @highLightEl

  render: ->
    @$el.html JST['desktops/list_item'](@model.attributes)
    @$('.actions').html JST['desktops/actions']
    @

  handleElClick: (e) ->
    e.preventDefault()
    @listView.renderDesktop @model
    StickyNotes.app.navigate 'desktops/'+@model.id

  highLightEl: ->
    @$el.siblings().removeClass 'current'
    @$el.addClass 'current'

  handleDestroyClick: (e) ->
    e.preventDefault()
    e.stopPropagation()
    @model.destroy success: => @remove()
