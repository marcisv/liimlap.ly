StickyNotes.NewDesktopView = Backbone.View.extend

  tagName: 'li'
  className: 'new-desktop'

  events:
    'keyup'       : 'handleCancelKeyup'
    'submit form' : 'handleFormSubmit'

  render: ->
    @$el.html JST['desktops/new']()
    setTimeout -> @$('input.desktop-title').focus()
    @

  handleCancelKeyup: (e) ->
    @remove() if e.keyCode == 27

  handleFormSubmit: (e) ->
    e.preventDefault()
    desktopTitle = @$('input.desktop-title').val()
    if desktopTitle.length
      @collection.create title: desktopTitle, {wait: true}
      @remove()
    else
      alert('Desktop title can\'t be empty!')
