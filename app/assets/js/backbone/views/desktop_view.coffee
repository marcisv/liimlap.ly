StickyNotes.DesktopView = Backbone.View.extend

  id: 'current-desktop'

  noteViews: []

  initialize: ->
    _.bindAll @, 'renderNote'
    @render()
    @model.noteList.on 'add', @renderNote

  render: ->
    _.each @model.notes(), (note) => @renderNote note
    @

  renderNote: (note) ->
    noteView = new StickyNotes.NoteView(model: note)
    @$el.append noteView.render()
    @noteViews.push noteView

  remove: ->
    _.each @noteViews, (noteView) -> noteView.remove()
    Backbone.View::remove.apply @
