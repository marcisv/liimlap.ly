StickyNotes.NoteView = Backbone.View.extend

  events:
    'click a.edit-note'    : 'handleEditClick'
    'keyup'                : 'handleCancelKeyup'
    'click a.destroy-note' : 'handleDestroyClick'
    'submit form'          : 'handleFormSubmit'

  render: ->
    @setElement JST['notes/show'](@model.attributes)
    @renderActions()
    @renderEdit() if @model.isNew()
    @bindDraggable()
    @$el

  $desktop: -> $('#current-desktop-wrapper')

  bindDraggable: ->
    @$el.draggable
      containment: @$desktop()
      stop: (event, ui) =>
        @model.updatePosition ui.helper.offset(), @$desktop().offset(), @$desktop().width(), @$desktop().height()

  handleEditClick: (e) ->
    e.preventDefault()
    @renderEdit()

  handleDestroyClick: (e) ->
    e.preventDefault()
    @model.destroy success: => @remove()

  renderEdit: ->
    @$('.note').replaceWith JST['notes/edit'](@model.attributes)
    setTimeout -> @$('input[type=text]').focus()

  handleCancelKeyup: (e) ->
    @renderContent() if e.keyCode == 27

  renderContent: ->
    @$('form').replaceWith JST['notes/content'](@model.attributes)
    @renderActions()

  renderActions: ->
    @$('.actions').html JST['notes/actions']

  handleFormSubmit: (e) ->
    e.preventDefault()
    $field = @$('input[type=text]')
    attributes = {}
    attributes[$field.attr('name')] = $field.val()
    @model.save attributes, success: =>
      @renderContent()
