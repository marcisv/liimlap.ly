StickyNotes.Router = Backbone.Router.extend

  routes:
    '' :             'workspace'
    'desktops/:id' : 'workspace'

  workspace: ->
    @desktopList = new StickyNotes.DesktopList(StickyNotes.bootstrappedDesktopAttributes)
    @workspace = new StickyNotes.WorkspaceView(collection: @desktopList)
    @workspace.render()
