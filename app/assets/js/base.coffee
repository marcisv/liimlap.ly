$ -> StickyNotes.launch()

@StickyNotes =

  app: null

  launch: ->
    @app = new StickyNotes.Router()
    Backbone.history.start pushState: true
